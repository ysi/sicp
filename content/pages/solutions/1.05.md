title: SICP Exercise 1.05
slug: sicp-exercise-1-05

Applicative-order evaluation
----------------------------

Code
```
:::scheme
#lang racket

(define (p) (p))
(define (test x y) (if (= x 0) 0 (p)))

; tests

; process doesn't stop for applicative-order evaluation
; (test 0 (p))
```

Normal-order evaluation
-----------------------

Code
```
:::scheme
#lang lazy

(define (p) (p))
(define (test x y) (if (= x 0) 0 (p)))

; tests

; normal-order evaluation (lazy evaluation)
(test 0 (p))
```

Output
```
:::text
0
```
