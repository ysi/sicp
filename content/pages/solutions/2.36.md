title: SICP Exercise 2.36
slug: sicp-exercise-2-36

Code
```
:::scheme
#lang racket

(require "common.scm")

(accumulate-n
 +
 0
 (list (list 1 2 3) (list 4 5 6) (list 7 8 9) (list 10 11 12)))
```

Output
```
:::text
'(22 26 30)
```
