title: SICP Exercise 1.40
slug: sicp-exercise-1-40

Code
```
:::scheme
#lang racket

(require "common.scm")

;; Fixed points

(define tolerance 0.00001)
(define dx tolerance)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (deriv g)
  (lambda (x) (/ (- (g (+ x dx)) (g x)) dx )))

(define (newton-transform g)
  (lambda (x) (- x (/ (g x) ((deriv g) x)))))

(define (newtons-method g guess)
  (fixed-point (newton-transform g) guess))

(define (sqrt x)
  (newtons-method
   (lambda (y) (- (square y) x)) 1.0))

; the resulting value is a fixed point of the transformed function
(define (fixed-point-of-transform g transform guess)
  (fixed-point (transform g) guess))

(define (tsqrt x)
  (fixed-point-of-transform
   (lambda (y) (/ x y))
   average-damp
   1.0))

(define (nsqrt x)
  (fixed-point-of-transform
   (lambda (y) (- (square y) x)) newton-transform 1.0))

(define (cubic a b c)
  (lambda (x) (+
               (cube x)
               (* a (square x))
               (* b x)
               c)))

(newtons-method (cubic 1 -10 1) 50)
(newtons-method (cubic 1 -10 1) 10)
(newtons-method (cubic 1 -10 1) 0)
(newtons-method (cubic 1 -10 1) 1)
(newtons-method (cubic 1 -10 1) -10)
(newtons-method (cubic 1 -10 1) -50)
```

Output
```
:::text
2.641891682727518
2.6418916827276204
0.10112606446853241
0.10112606447108206
-3.7430177471902093
-3.7430177471853097
```
