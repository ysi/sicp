title: SICP Solutions
slug: sicp-solutions
category: SICP

My answers to the exercises in Structure and Interpretation of Computer Programs (SICP).
The code can be downloaded at [github](https://github.com/voom4000/sicp). It was tested with [Dr.Racket](http://racket-lang.org/), ver 6.2.

There are many other SICP solutions in the Internet. Some exercises are easy, others are challenging and/or admit a number of cool solutions.
Let’s try to find more of them!


## Common files


Some simple common procedures were placed in this file:

[common.scm]({filename}common.md)


## Cool stuff


[Recursive Tower of Hanoi]({filename}recursive.hanoi.md)

[Iterative Tower of Hanoi]({filename}iterative.hanoi.md)

[Counting change]({filename}counting.change.md)

[Interval arithmetic]({filename}interval.arithmetic.md)


## Chapter 1


[1.01]({filename}1.01.md)
[1.02]({filename}1.02.md)
[1.03]({filename}1.03.md)
[1.04]({filename}1.04.md)
[1.05]({filename}1.05.md)
[1.06]({filename}1.06.md)
[1.07]({filename}1.07.md)
[1.08]({filename}1.08.md)
[1.09]({filename}1.09.md)
[1.10]({filename}1.10.md)

[1.11]({filename}1.11.md)
[1.12]({filename}1.12.md)
[1.13]({filename}1.13.md)
[1.14]({filename}1.14.md)
[1.15]({filename}1.15.md)
[1.16]({filename}1.16.md)
[1.17]({filename}1.17.md)
[1.18]({filename}1.18.md)
[1.19]({filename}1.19.md)
[1.20]({filename}1.20.md)

[1.21]({filename}1.21.md)
[1.22]({filename}1.22.md)
[1.23]({filename}1.23.md)
[1.24]({filename}1.24.md)
[1.25]({filename}1.25.md)
[1.26]({filename}1.26.md)
[1.27]({filename}1.27.md)
[1.28]({filename}1.28.md)
[1.29]({filename}1.29.md)
[1.30]({filename}1.30.md)

[1.31]({filename}1.31.md)
[1.32]({filename}1.32.md)
[1.33]({filename}1.33.md)
[1.34]({filename}1.34.md)
[1.35]({filename}1.35.md)
[1.36]({filename}1.36.md)
[1.37]({filename}1.37.md)
[1.38]({filename}1.38.md)
[1.39]({filename}1.39.md)
[1.40]({filename}1.40.md)

[1.41]({filename}1.41.md)
[1.42]({filename}1.42.md)
[1.43]({filename}1.43.md)
[1.44]({filename}1.44.md)
[1.45]({filename}1.45.md)
[1.46]({filename}1.46.md)



## Chapter 2

[2.01]({filename}2.01.md)
[2.02]({filename}2.02.md)
[2.03]({filename}2.03.md)
[2.04]({filename}2.04.md)
[2.05]({filename}2.05.md)
[2.06]({filename}2.06.md)
[2.07]({filename}2.07.md)
[2.08]({filename}2.08.md)
[2.09]({filename}2.09.md)
[2.10]({filename}2.10.md)

[2.11]({filename}2.11.md)
[2.12]({filename}2.12.md)
[2.13]({filename}2.13.md)
[2.14]({filename}2.14.md)
[2.15]({filename}2.15.md)
[2.16]({filename}2.16.md)
[2.17]({filename}2.17.md)
[2.18]({filename}2.18.md)
[2.19]({filename}2.19.md)
[2.20]({filename}2.20.md)

[2.21]({filename}2.21.md)
[2.22]({filename}2.22.md)
[2.23]({filename}2.23.md)
[2.24]({filename}2.24.md)
[2.25]({filename}2.25.md)
[2.26]({filename}2.26.md)
[2.27]({filename}2.27.md)
[2.28]({filename}2.28.md)
[2.29]({filename}2.29.md)
[2.30]({filename}2.30.md)

[2.31]({filename}2.31.md)
[2.32]({filename}2.32.md)
[2.33]({filename}2.33.md)
[2.34]({filename}2.34.md)
[2.35]({filename}2.35.md)
[2.36]({filename}2.36.md)
[2.37]({filename}2.37.md)
[2.38]({filename}2.38.md)
[2.39]({filename}2.39.md)
[2.40]({filename}2.40.md)

[2.41]({filename}2.41.md)
[2.42]({filename}2.42.md)
[2.43]({filename}2.43.md)
[2.44]({filename}2.44.md)
[2.45]({filename}2.45.md)
[2.46]({filename}2.46.md)
[2.47]({filename}2.47.md)
[2.48]({filename}2.48.md)
[2.49]({filename}2.49.md)
[2.50]({filename}2.50.md)

[2.51]({filename}2.51.md)
[2.52]({filename}2.52.md)
[2.53]({filename}2.53.md)
[2.54]({filename}2.54.md)
[2.55]({filename}2.55.md)
[2.56]({filename}2.56.md)
[2.57]({filename}2.57.md)
[2.58]({filename}2.58.md)
[2.59]({filename}2.59.md)
[2.60]({filename}2.60.md)

[2.61]({filename}2.61.md)
[2.62]({filename}2.62.md)
[2.63]({filename}2.63.md)
[2.64]({filename}2.64.md)
[2.65]({filename}2.65.md)
[2.66]({filename}2.66.md)
[2.67]({filename}2.67.md)
[2.68]({filename}2.68.md)
[2.69]({filename}2.69.md)
[2.70]({filename}2.70.md)

[2.71]({filename}2.71.md)
[2.72]({filename}2.72.md)
2.73 2.74 2.75 2.76 2.77 2.78 2.79 2.80

2.81 2.82 2.83 2.84 2.85 2.86 2.87 2.88 2.89 2.90

2.91 2.92 2.93 2.94 2.95 2.96 2.97



## Chapter 3


3.01 3.02 3.03 3.04 3.05 3.06 3.07 3.08 3.09 3.10

3.11 3.12 3.13 3.14 3.15 3.16 3.17 3.18 3.19 3.20

3.21 3.22 3.23 3.24 3.25 3.26 3.27 3.28 3.29 3.30

3.31 3.32 3.33 3.34 3.35 3.36 3.37 3.38 3.39 3.40

3.41 3.42 3.43 3.44 3.45 3.46 3.47 3.48 3.49 3.50

3.51 3.52 3.53 3.54 3.55 3.56 3.57 3.58 3.59 3.60

3.61 3.62 3.63 3.64 3.65 3.66 3.67 3.68 3.69 3.70

3.71 3.72 3.73 3.74 3.75 3.76 3.77 3.78 3.79 3.80

3.81 3.82



## Chapter 4


4.01 4.02 4.03 4.04 4.05 4.06 4.07 4.08 4.09 4.10

4.11 4.12 4.13 4.14 4.15 4.16 4.17 4.18 4.19 4.20

4.21 4.22 4.23 4.24 4.25 4.26 4.27 4.28 4.29 4.30

4.31 4.32 4.33 4.34 4.35 4.36 4.37 4.38 4.39 4.40

4.41 4.42 4.43 4.44 4.45 4.46 4.47 4.48 4.49 4.50

4.51 4.52 4.53 4.54 4.55 4.56 4.57 4.58 4.59 4.60

4.61 4.62 4.63 4.64 4.65 4.66 4.67 4.68 4.69 4.70

4.71 4.72 4.73 4.74 4.75 4.76 4.77 4.78 4.79



## Chapter 5


5.01 5.02 5.03 5.04 5.05 5.06 5.07 5.08 5.09 5.10

5.11 5.12 5.13 5.14 5.15 5.16 5.17 5.18 5.19 5.20

5.21 5.22 5.23 5.24 5.25 5.26 5.27 5.28 5.29 5.30

5.31 5.32 5.33 5.34 5.35 5.36 5.37 5.38 5.39 5.40

5.41 5.42 5.43 5.44 5.45 5.46 5.47 5.48 5.49 5.50

5.51 5.52
