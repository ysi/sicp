title: SICP Exercise 1.30
slug: sicp-exercise-1-30

Code
```
:::scheme
#lang racket

(require "common.scm")

(define (sum term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (+ result (term a)))))
  (iter a 0))

(define (sum-cubes a b)
  (sum cube a inc b))

; tests

(sum-cubes 1 10)
```

Output
```
:::text
3025
```
