title: SICP Exercise 2.31
slug: sicp-exercise-2-31

Code
```
:::scheme
#lang racket

(require "common.scm")

(define (square-tree tree) (tree-map square tree))

(define (tree-map action tree)
  (map (lambda (sub-tree)
         (if (pair? sub-tree)
             (tree-map action sub-tree)
             (action sub-tree)))
       tree))

; tests

(define tree (list 1 (list 2 (list 3 4) 5) (list 6 7)))
tree
(square-tree tree)
```

Output
```
:::text
'(1 (2 (3 4) 5) (6 7))
'(1 (4 (9 16) 25) (36 49))
```
