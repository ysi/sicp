title: SICP Exercise 2.55
slug: sicp-exercise-2-55

Code
```
:::scheme
#lang racket

(car ''abracadabra)
(car (quote 'abracadabra))
(car (quote (quote abracadabra)))
(car '(quote abracadabra))
```

Output
```
:::text
'quote
'quote
'quote
'quote
```
