title: SICP Exercise 2.17
slug: sicp-exercise-2-17

Code
```
:::scheme
#lang racket

(define (last-pair items)
  (list-ref items (- (length items) 1)))

; tests

(last-pair (list 1 2 3 4))

```

Output
```
:::text
4
```
