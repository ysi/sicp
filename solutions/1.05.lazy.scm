#lang lazy

(define (p) (p))
(define (test x y) (if (= x 0) 0 (p)))

; tests

; normal-order evaluation (lazy evaluation)
(test 0 (p))
