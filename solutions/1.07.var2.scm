#lang racket

(require "common.scm")

; second variant

(define (sqrt-iter oldguess guess x)
  (if (good-enough? oldguess guess)
      guess
      (sqrt-iter guess (improve guess x) x)))

(define (improve guess x)
  (average guess (/ x guess)))

(define (good-enough? oldguess guess)
  (< (/ (abs (- oldguess guess)) guess) 1e-10))

(define (mysqrt x) (sqrt-iter 2.0 1.0 x))

(define (test radicand)
  (define calculated (mysqrt radicand))
  (define exact (sqrt radicand))
  (display "radicand: ")
  (display radicand)
  (newline)
  (display "sqrt exact: ")
  (display exact)
  (newline)
  (display "sqrt calculated: ")
  (display calculated)
  (newline))

; tests

(test 1e-80)
(test 1e+80)
