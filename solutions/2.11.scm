#lang racket

(define make-interval cons)
(define lower-bound car)
(define upper-bound cdr)

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define (new-mul-interval x y)
  (define x1 (lower-bound x))
  (define x2 (upper-bound x))
  (define y1 (lower-bound y))
  (define y2 (upper-bound y))
  (cond
    ((and (<= 0 x1)  (<= x1 x2) (<= 0 y1)  (<= y1 y2)) (make-interval (* x1 y1) (* x2 y2)))
    ((and (<= x1 0)  (<= 0 x2)  (<= 0 y1)  (<= y1 y2)) (make-interval (* x1 y2) (* x2 y2)))
    ((and (<= x1 x2) (<= x2 0)  (<= 0 y1)  (<= y1 y2)) (make-interval (* x1 y2) (* x2 y1)))
    ((and (<= 0 x1)  (<= x1 x2) (<= y1 0)  (<= 0 y2))  (make-interval (* x2 y1) (* x2 y2)))
    ((and (<= x1 x2) (<= x2 0)  (<= y1 0)  (<= 0 y2))  (make-interval (* x1 y2) (* x1 y1)))
    ((and (<= 0 x1)  (<= x1 x2) (<= y1 y2) (<= y2 0))  (make-interval (* x2 y1) (* x1 y2)))
    ((and (<= x1 0)  (<= 0 x2)  (<= y1 y2) (<= y2 0))  (make-interval (* x2 y1) (* x1 y1)))
    ((and (<= x1 x2) (<= x2 0)  (<= y1 y2) (<= y2 0))  (make-interval (* x2 y2) (* x1 y1)))
    ;((and (<= x1 0)  (<= 0 x2)  (<= y1 0)  (<= 0 y2))  (mul-interval x y))
    (else (mul-interval x y))))


(define (equal-intervals? r1 r2)
  (and
   (= (lower-bound r1)
      (lower-bound r2))
   (= (upper-bound r1)
      (upper-bound r2))))

; tests

(define (same-mul? i1 i2)
  (define r1 (new-mul-interval i1 i2))
  (define r2 (mul-interval i1 i2))
  (equal-intervals? r1 r2))

(define i1 (make-interval -10 -5))
(define i2 (make-interval -10 5))
(define i3 (make-interval 5 10))

(same-mul? i1 i1)
(same-mul? i2 i1)
(same-mul? i3 i1)
(same-mul? i1 i2)
(same-mul? i2 i2)
(same-mul? i3 i2)
(same-mul? i1 i3)
(same-mul? i2 i3)
(same-mul? i3 i3)