#lang racket

; ordered set with no duplicates

(define (element-of-set? x set)
  (cond ((null? set) false)
        ((= x (car set)) true)
        ((< x (car set)) false)
        (else (element-of-set? x (cdr set)))))

(define (intersection-set set1 set2)
  (if (or (null? set1) (null? set2))
      '()
      (let ((x1 (car set1)) (x2 (car set2)))
        (cond ((= x1 x2) (cons x1 (intersection-set (cdr set1) (cdr set2))))
              ((< x1 x2) (intersection-set (cdr set1) set2))
              ((< x2 x1) (intersection-set set1 (cdr set2)))))))

(define (adjoin-set x set)
  (cond
    ((null? set) (list x))
    ((= x (car set) set))
    ((< x (car set)) (cons x set))
    (else (cons (car set) (adjoin-set x (cdr set))))))

; tests

(define set1 '(1 2 3 4))
(define set2 '(3 4 5 6))
(intersection-set set1 set2)

(adjoin-set 0 set1)
(adjoin-set 2.5 set1)
(adjoin-set 3 set1)
