#lang racket

(require "common.scm")

(define (dot-product v w)
  (accumulate + 0 (map * v w)))

(define (matrix-*-vector m v)
  (map (lambda (row) (dot-product row v)) m))

(define (transpose m)
  (accumulate-n cons '() m))

(define (matrix-*-matrix m n)
  (let ((cols (transpose n)))
    (map
     (lambda (rowa) (map (lambda (rowb) (dot-product rowa rowb)) cols))
     m)))

; tests

(define m1 (list (list 1 0 0) (list 0 1 0) (list 0 0 1)))
(define v1 (list 1 2 3))
(matrix-*-vector m1 v1)
(newline)
(define m2 (list (list 1 2 3) (list 4 5 6) (list 7 8 9)))
(transpose m2)
(newline)
(matrix-*-matrix m1 m1)
(matrix-*-matrix m1 m2)
(matrix-*-matrix m2 m1)
(matrix-*-matrix m2 m2)
