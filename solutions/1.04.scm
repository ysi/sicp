#lang racket

(define (a-plus-abs-b a b)
  ((if (> b 0) + -) a b))

; tests

(a-plus-abs-b 1 2)
(a-plus-abs-b 1 -2)
