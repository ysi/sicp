#lang racket

(require "picture.language.scm")
(require "picture.window.scm")

; the other definition of "below" is
; in the file "picture.language.scm"

(define (below1 painter1 painter2)
  (rotate270 (rotate180 (beside (rotate270 painter1) (rotate270 painter2)))))

; tests

((below1 wave wave2) unit-frame)