#lang racket

(require "picture.language.scm")

(require graphics/graphics)
(open-graphics)
(define vp (open-viewport "A Picture Language" 600 600))

(define unit-frame
  (make-frame
   (make-vect 0 600)
   (make-vect 600 0)
   (make-vect 0 -600)))
(provide unit-frame)

(define (draw-line-1 start-vect end-vect)
  ((draw-line vp)
   (make-posn (xcor-vect start-vect)
              (ycor-vect start-vect))
   (make-posn (xcor-vect end-vect)
              (ycor-vect end-vect))))

(define (segments->painter segment-list)
  (lambda (frame)
    (for-each
     (lambda (segment)
       (draw-line-1
        ((frame-coord-map frame)
         (start-segment segment))
        ((frame-coord-map frame)
         (end-segment segment))))
     segment-list)))
(provide segments->painter)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Painters
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (connect vect-list)
  (define (iter segment-list remaining)
    (if (null? (cdr remaining))
        (reverse segment-list)
        (iter (cons (make-segment
                     (car remaining)
                     (cadr remaining))
                    segment-list)
              (cdr remaining))))
  (iter '() vect-list))
(provide connect)

(define (outline frame)
  ((segments->painter 
    (connect
     (list
      (make-vect 0 0)
      (make-vect 1 0)
      (make-vect 1 1)
      (make-vect 0 1)
      (make-vect 0 0))
     ))
   frame))
(provide outline)

(define (x-painter frame)
  ((segments->painter
    (list
     (make-segment (make-vect 0 0) (make-vect 1 1))
     (make-segment (make-vect 0 1) (make-vect 1 0))
     ))
   frame))
(provide x-painter)

(define (diamond frame)
  (let ((start (make-vect 0.5 0)))
    ((segments->painter 
      (connect 
       (list
        start
        (make-vect 1 0.5)
        (make-vect 0.5 1)
        (make-vect 0 0.5)
        start)))
     frame)))
(provide diamond)

(define (wave frame)
  ((segments->painter
    (append
     (connect
      (list (make-vect 0.4  0.0)
            (make-vect 0.5  0.33)
            (make-vect 0.6  0.0))) ; inside legs
     
     (connect
      (list (make-vect 0.25 0.0)
            (make-vect 0.42 0.55)
            (make-vect 0.3  0.55)
            (make-vect 0.1  0.5)
            (make-vect 0.0  0.65))) ; lower left
     
     (connect
      (list (make-vect 0.0  0.72)
            (make-vect 0.1  0.6)
            (make-vect 0.33 0.68)
            (make-vect 0.45  0.65)
            (make-vect 0.4 0.8)
            (make-vect 0.45  1.0))) ; upper left
     
     (connect
      (list (make-vect 0.75 0.0)
            (make-vect 0.58  0.55)
            (make-vect 0.65  0.55)
            (make-vect 1.0  0.28))) ; lower right
     
     (connect
      (list (make-vect 1.0  0.35)
            (make-vect 0.68  0.68)
            (make-vect 0.55  0.65)
            (make-vect 0.6 0.8)
            (make-vect 0.55  1.0)
            (make-vect 0.45  1.0))) ; upper right
     ))
   frame))
(provide wave)

(define wave2 (beside wave (flip-vert wave)))
(provide wave2)

(define wave4 (below wave2 wave2))
(provide wave4)

(define empty-painter
  (lambda (frame) (display "")))
(provide empty-painter)
