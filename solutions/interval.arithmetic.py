import random

# intervals
intA = (2, 4)
intB = (-2, 0)
intC = (10, 20.5)

# function of three intervals
def f(A, B, C):
    #return (A+B)/(A+C)
    return (A - 2) * (A - 4)

# calculation of the resulting interval
N = 1000000
minf = 1e80
maxf = -1e80
for i in range(N):
    A = random.uniform(intA[0], intA[1])
    B = random.uniform(intB[0], intB[1])
    C = random.uniform(intC[0], intC[1])
    res = f(A, B, C)
    if minf > res: minf = res
    if maxf < res: maxf = res

print("Monte-Carlo method")
print("min = %6.2f" % (minf))
print("max = %6.2f" % (maxf))
