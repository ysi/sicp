#lang racket

(define (f-rec n)
  (if (< n 3)
      n
      (+ (f-rec (- n 1))
         (* 2 (f-rec (- n 2)))
         (* 3 (f-rec (- n 3))))))


(define (f-iter n)
  (define (iter a1 a2 a3 count)
    (if (= count 2)
        a3
        (iter 
         a2 
         a3
         (+ a3
            (* 2 a2)
            (* 3 a1))
         (- count 1))))
    (if (< n 3)
        n
        (iter 0 1 2 n)))

; tests

(f-rec 28)
(f-iter 28)
