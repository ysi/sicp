#lang racket

(require "common.scm")

(define (sum term a next b)
  (if (> a b)
      0
      (+ (term a)
         (sum term (next a) next b))))

(define (sum-cubes a b)
  (sum cube a inc b))

(define (rectangles f a b dx)
  (define (add-dx x) (+ x dx))
  (* (sum f (+ a (/ dx 2)) add-dx b)
     dx))

(define (simpson f a b n)
  (define h (/ (- b a) n))
  (define (func k) 
    (define yk (f (+ a (* k h))))
    (cond
      ((or (= k 0) (= k n)) yk)
      ((even? k) (* 2 yk))
      (else (* 4 yk))))
  (* h (/ (sum func 0 inc n) 3)))

; tests

(rectangles cube 0 1 0.01)
(rectangles cube 0 1 0.001)

(simpson cube 0. 1. 100)
(simpson cube 0. 1. 1000)
