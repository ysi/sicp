#lang racket

(require "common.scm")

;; Fixed points (modified)

(define tolerance 0.00001)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess count)
    (display count)
    (display " ")
    (display guess)
    (newline)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next (+ 1 count)))))
  (try first-guess 0))

; tests

(fixed-point (lambda (x) (/ (log 1000) (log x))) 2.0) ; w/o average damping, 33 steps
(newline)
(fixed-point (lambda (x) (average x (/ (log 1000) (log x)))) 2.0) ; with average damping, 8 steps
