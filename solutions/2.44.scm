#lang racket

(require "picture.language.scm")
(require "picture.window.scm")

; these definitions of right-split1 and up-split1
; are according to the excercise

; the file "picture.language.scm"
; contains their different implementations

(define (right-split1 painter n)
  (if (= n 0)
      painter
      (let ((smaller (right-split1 painter (- n 1))))
        (beside painter (below smaller smaller)))))

(define (up-split1 painter n)
  (if (= n 0)
      painter
      (let ((smaller (up-split1 painter (- n 1))))
        (below painter (beside smaller smaller)))))

; tests

;((right-split1 wave 4) unit-frame)
;((up-split1 wave 4) unit-frame)
