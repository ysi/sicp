#lang racket

(define (make-rat n d)
  (define g ((if (< d 0) - +) (gcd n d)))
    (cons (/ n g) (/ d g)))

(define numer car)
(define denom cdr)

; tests

(make-rat 6 9)
(make-rat -6 9)
(make-rat 6 -9)
(make-rat -6 -9)
