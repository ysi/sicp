#lang racket

(define us-coins (list 50 25 10 5 1))
(define us-coins1 (reverse (list 50 25 10 5 1)))
(define uk-coins (list 100 50 20 10 5 2 1 0.5))


(define (except-first-denomination items)
  (cdr items))

(define (first-denomination items)
  (car items))

(define (no-more? items)
  (null? items))

(define (cc amount coin-values)
  (cond ((= amount 0) 1)
        ((or (< amount 0) (no-more? coin-values)) 0)
        (else
         (+ (cc amount
                (except-first-denomination coin-values))
            (cc (- amount
                   (first-denomination coin-values))
                coin-values)))))

; tests

(cc 100 us-coins)
(cc 100 (reverse us-coins))

(cc 100 uk-coins)
(cc 100 (reverse uk-coins)) ; takes more time

; the order does not affect the answer
; all combinations are probed for any order
; when large coin values are near the end of the list, there are more
; wrong tries with amount<0 and the time of calculation is larger
