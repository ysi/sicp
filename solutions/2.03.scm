#lang racket

; as in ex. 2.2
(define x-point car)
(define y-point cdr)
(define make-point cons)
(define start-segment car)
(define end-segment cdr)
(define make-segment cons)


; first representation
(define start-rect start-segment)
(define end-rect end-segment)
(define make-rect make-segment)

; second representation
;(define start-rect cdr)
;(define end-rect car)
;(define (make-rect x y) (cons y x))

(define (area-rect rect)
  (* (abs (- (x-point (start-rect rect))
             (x-point (end-rect rect))))
     (abs (- (y-point (start-rect rect))
             (y-point (end-rect rect))))))

(define (perimeter-rect rect)
  (* 2 (+ (abs (- (x-point (start-rect rect))
             (x-point (end-rect rect))))
            (abs (- (y-point (start-rect rect))
             (y-point (end-rect rect)))))))

; tests

(define r (make-rect (make-point -5 -8) (make-point 3 10)))
(area-rect r)
(perimeter-rect r)