#lang racket

(require "common.scm")

(define (rem a b)
  ;(display a)
  ;(newline)
  (remainder a b))

(define (expmod2 base exp m)
  (display 1)
  (newline)
  (cond ((= exp 0) 1)
        ((even? exp)
         (rem (* (expmod2 base (/ exp 2) m)
                 (expmod2 base (/ exp 2) m))
              m))
        (else
         (rem (* base (expmod2 base (- exp 1) m))
              m))))

(define (expmod1 base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (rem (square (expmod1 base (/ exp 2) m))
                    m))
        (else
         (rem (* base (expmod1 base (- exp 1) m))
                    m)))) 

;(expmod1 2 16 10)
(expmod2 2 1 10)
