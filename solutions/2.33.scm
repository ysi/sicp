#lang racket

(require "common.scm")

(define (map1 p sequence)
  (accumulate (lambda (x y) (cons (p x) y)) '() sequence))

(define (append1 seq1 seq2)
  (accumulate cons seq2 seq1))

(define (lenght1 sequence)
  (accumulate  (lambda (x y) (+ 1 y)) 0 sequence))

; tests

(map1 square (list 1 2 3 4))
(append1 (list 1 2 3) (list 4 5 6))
(lenght1 (list))
(lenght1 (list 1))
(lenght1 (list 1 2))
