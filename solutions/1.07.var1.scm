#lang racket

(require "common.scm")

; first variant

(define (sqrt-iter guess x)
  (if (good-enough? guess x)
      guess
      (sqrt-iter (improve guess x) x)))

(define (improve guess x)
  (average guess (/ x guess)))

(define (good-enough? guess x)
  (< (abs (- (square guess) x)) 1e-3))

(define (mysqrt x) (sqrt-iter 1.0 x))

(define (test radicand)
  (define calculated (mysqrt radicand))
  (define exact (sqrt radicand))
  (display "radicand: ")
  (display radicand)
  (newline)
  (display "sqrt exact: ")
  (display exact)
  (newline)
  (display "sqrt calculated: ")
  (display calculated)
  (newline))


; tests

(test 1e-80)

;(test 1e+80)
; process doesn't stop
