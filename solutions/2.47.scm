#lang racket

(require "picture.language.scm")

; One realization of constructor/selectors
; is in "picture.language.scm".

; Here is the other one.

(define (make-frame1 origin edge1 edge2)
  (list origin edge1 edge2))

(define (origin-frame1 frame)
  (car frame))

(define (edge1-frame1 frame)
  (cadr frame))

(define (edge2-frame1 frame)
  (caddr frame))

(define (frame-coord-map1 frame)
  (lambda (v) 
    (add-vect
     (origin-frame1 frame)
     (add-vect 
      (scale-vect (xcor-vect v) (edge1-frame1 frame))
      (scale-vect (ycor-vect v) (edge2-frame1 frame))))))

; tests

(define o (make-vect 0.1 0.2))
(define e1 (make-vect 0.3 0.4))
(define e2 (make-vect 0.5 0.6))

(define f (make-frame o e1 e2))
(define f1 (make-frame1 o e1 e2))

(origin-frame f)
(origin-frame1 f1)
(edge1-frame f)
(edge1-frame1 f1)
(edge2-frame f)
(edge2-frame1 f1)
((frame-coord-map f) (make-vect 1 1))
((frame-coord-map1 f1) (make-vect 1 1))
