#lang racket

(define x (list 1 2 3))
(define y (list 4 5 6))

; tests

(append x y) ; list of integers
(cons x y) ; tree
(list x y) ; list of lists
