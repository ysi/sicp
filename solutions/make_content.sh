#!/bin/bash

scm2out () {
  for n in $@; do
    echo "racket ${n}.scm > ${n}.out"
    racket ${n}.scm > ${n}.out
  done
}

py2out () {
  for n in $@; do
    echo "python ${n}.py > ${n}.out"
    python ${n}.py > ${n}.out
  done
}

mdi2md () {
  for n in $@; do
    echo "python include.py ${n}.mdi > ../content/pages/solutions/${n}.md"
    python include.py ${n}.mdi > ../content/pages/solutions/${n}.md
  done
}

##########################################################################

#py2out iterative.change memoization.change interval.arithmetic

#scm2out recursive.hanoi iterative.hanoi
#scm2out 1.01 1.02 1.03 1.04 1.05.lazy 1.06
#scm2out 1.07.var1 1.07.var2 1.08 1.09 1.10
#scm2out 1.11 1.12 1.15 1.16 1.17 1.18 1.19
#scm2out 1.21 1.22 1.23 1.24 1.25 1.26 1.27 1.28 1.29 1.30
#scm2out 1.31 1.32 1.33 1.35 1.36 1.37 1.38 1.39 1.40
#scm2out 1.41 1.42 1.43 1.44 1.45 1.46
#scm2out 2.01 2.02 2.03 2.04 2.05 2.06 2.11 2.14
#scm2out 2.17 2.18 2.19 2.20 2.21
#scm2out 2.23 2.24 2.25 2.26 2.27 2.28 2.29.rep1 2.29.rep2 2.30
#scm2out 2.31 2.32 2.33 2.34 2.35 2.36 2.37 2.38 2.39 2.40 2.41
#scm2out 2.42 2.43 2.46 2.47 2.48
#scm2out 2.53 2.54 2.55 2.56 2.57 2.58
#scm2out 2.59 2.60 2.61 2.62 2.63 2.64 2.65 2.66
scm2out 2.67 2.68 2.69 2.70 2.71

mdi2md solutions common interval.arithmetic
#mdi2md recursive.hanoi iterative.hanoi counting.change

#for i in $(seq 1 46); do
#    mdi2md 1.$(printf "%02d" $i)
#done

#for i in $(seq 1 58); do
#    mdi2md 2.$(printf "%02d" $i)
#done

for i in $(seq 59 72); do
    mdi2md 2.$(printf "%02d" $i)
done
