# recursive counting change with memoization
# super optimal algorithm

# amount
a = 100

# denominations
m = [1,5,10,25,50]

# storage for memoization
dictionary = {}

calls = 0 # number of calls
depth = 0 # current tree depth
maxdepth = 0 # maximal tree depth

def memo_change(a, kinds):
	global dictionary
	global calls, depth, maxdepth

	pair = (a, kinds)
	if pair in dictionary.keys():
		return dictionary[pair]

	depth = depth + 1
	if maxdepth < depth:
		maxdepth = depth
	calls = calls + 1

	ways1 = 0
	if a > m[kinds-1]:
		ways1 = memo_change(a - m[kinds-1], kinds)
	else:
		if a == m[kinds-1]:
			ways1 = 1

	ways2 = 0
	if kinds>2:
		ways2 = memo_change(a, kinds - 1)
	else:
		if a % m[kinds - 1] == 0:
			ways2 = 1

	ways = ways1 + ways2

	dictionary[pair] = ways

	depth = depth - 1
	return ways


print "amount = ", a, "  ways = ", memo_change(a, len(m)), \
"  calls = ", calls, "  max depth = ", maxdepth
