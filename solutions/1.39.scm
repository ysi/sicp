#lang racket

(require "common.scm")

; iterative
(define (it-cont-frac D N count)
  (define (iter result count)
    (if (= count 0)
        result
        (iter (/ (N count) (+ (D count) result)) (- count 1))))
  (iter 0 count))

(define (show D count)
  (display count)
  (display " ")
  (display (D count))
  (newline)
  (if (= count 0)
      0
      (show D (- count 1))))

(define (tan-cf x k)
  (define (N i)
    (if (= i 1)
        x
        (- (square x))))
  (define (D i)
    (- (* 2.0 i) 1))
  ;(show D 11)
  ;(show N 11)
  (it-cont-frac D N 20))

; tests

(tan-cf pi 10) ; almost zero
