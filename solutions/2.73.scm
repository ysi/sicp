#lang racket

(require "common.scm")

(define (install-deriv-package)

  ; derivative of sum
  
  (define (make-sum a1 a2)
    (cond ((=number? a1 0) a2)
          ((=number? a2 0) a1)
          ((and (number? a1) (number? a2)) (+ a1 a2))
          (else (list '+ a1 a2))))
  
  (put 'deriv '+ 
       (lambda (args var)
         (let ((addend (car args))
               (augend (if (> (length args) 2)
                           (cons '+ (cdr args))
                           (cadr args))))
           (make-sum (deriv addend var)
                     (deriv augend var)))))

  ; derivative of product

  (define (make-product m1 m2)
    (cond ((or (=number? m1 0) (=number? m2 0)) 0)
          ((=number? m1 1) m2)
          ((=number? m2 1) m1)
          ((and (number? m1) (number? m2)) (* m1 m2))
          (else (list '* m1 m2))))
  
  (put 'deriv '* (lambda (args var)
                   (let ((multiplier (car args))
                         (multiplicand (if (> (length args) 2)
                                           (cons '* (cdr args))
                                           (cadr args))))
                     (make-sum (make-product multiplier
                                             (deriv multiplicand var))
                               (make-product (deriv multiplier var)
                                             multiplicand)))))

  ; derivative of exponent
  
  (define (make-exponentiation base exp)
    (cond ((=number? exp 0) 1)
          ((=number? exp 1) base)
          ((=number? base 1) 1)
          ((and (number? base) (number? exp)) (expt base exp))
          (else (list '** base exp))))
  
  (put 'deriv '** (lambda (args var)
                    (let ((base (car args))
                          (exponent (cadr args)))
                      (make-product (make-product
                                     exponent
                                     (make-exponentiation base (make-sum exponent -1)))
                                    (deriv base var))))))

;;

(define (deriv exp var)
  (cond ((number? exp) 0)
        ((variable? exp) (if (same-variable? exp var) 1 0))
        (else ((get 'deriv (operator exp))
               (operands exp) var))))

(define (variable? x) (symbol? x))
(define (same-variable? v1 v2) (and (variable? v1) (variable? v2) (eq? v1 v2)))
(define (operator exp) (car exp))
(define (operands exp) (cdr exp))

; tests

(install-deriv-package)

(deriv '(* x y (+ x 3)) 'x)
(deriv '(* x x x) 'x)
(deriv '(+ (* x 3) (* 5 (+ x y 2))) 'x)

; a. because we cannot extract an operator from simple expressions, like numbers or variables.
; d. e.g (put '+ 'deriv  ...) instead of (put 'deriv '+ ...)