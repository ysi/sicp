#lang racket

(require "common.scm")

(define (sum term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (+ result (term a)))))
  (iter a 0))

(define (sum-cubes a b)
  (sum cube a inc b))


; recursive product
(define (product term a next b)
  (if (> a b)
      1
      (* (term a)
         (product term (next a) next b))))

(define (factorial n)
  (product identity 1 inc n))

; iterative product
(define (iter-product term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (* result (term a)))))
  (iter a 1))

(define (calc-pi n)
  (define (term i)
    (if (even? i)
        (/ (+ i 2.) (+ i 1))
        (/ (+ i 1.) (+ i 2))))
  (* 4 (iter-product term 1 inc n)))

; tests

(sum-cubes 1 10)
(factorial 5)
(iter-product identity 1 inc 5)
(display "calculated")
(newline)
(calc-pi 10000000)
(display "exact")
(newline)
pi
