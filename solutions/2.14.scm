#lang racket

(require "2.07.scm")
(require "2.10.scm")
(require "2.12.scm")

(define A (make-center-percent 100 0.01))
(define R (div-interval A A))

; tests

R
(width R)
(tolerance R)
