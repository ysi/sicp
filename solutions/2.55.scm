#lang racket

(car ''abracadabra)
(car (quote 'abracadabra))
(car (quote (quote abracadabra)))
(car '(quote abracadabra))
