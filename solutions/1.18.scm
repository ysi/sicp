#lang racket

(define (double n) (+ n n))

(define (halve n) (/ n 2))

(define (mult a b)
  (define (iter m a b)
    (cond ((= b 0) m)
          ((even? b) (iter m (double a) (halve b)))
          (else (iter (+ m a) a (- b 1)))))
  (iter 0 a b))

; tests

(mult 100 100)