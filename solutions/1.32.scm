#lang racket

(define (inc n) (+ n 1))

; recursive
(define (accumulate combiner null-value term a next b)
  (if (> a b)
      null-value
      (combiner (term a)
         (accumulate combiner null-value term (next a) next b))))

; iterative
(define (iter-accumulate combiner null-value term a next b)
  (define (iter a result)
    (if (> a b)
        result
        (iter (next a) (combiner result (term a)))))
  (iter a null-value))


(define (sum term a next b)
  (accumulate + 0 term a next b))

(define (product term a next b)
  (accumulate * 1 term a next b))

(define (iter-sum term a next b)
  (iter-accumulate + 0 term a next b))

(define (iter-product term a next b)
  (iter-accumulate * 1 term a next b))

; tests

(sum identity 1 inc 10)
(iter-sum identity 1 inc 10)
(product identity 1 inc 5)
(iter-product identity 1 inc 5)
