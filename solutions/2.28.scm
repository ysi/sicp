#lang racket

(define (fringe tree)
  (define (iter tree items)
    (cond ((null? tree) items)
          ((not (pair? tree)) (cons tree items))
          (else (iter (car tree) (iter (cdr tree) items)))))
  (iter tree '()))

; tests

(define x (list (list 1 2) (list 3 4)))
(fringe x)
(fringe (list x x))
(fringe (list x (list x x) (list x x)))
