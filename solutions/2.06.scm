#lang racket

(require "common.scm")

(define zero (lambda (f) (lambda (x) x)))

(define (add-1 n) (lambda (f) (lambda (x) (f ((n f) x)))))

(define one (add-1 zero))
(define myone (lambda (f) (lambda (x) (f x))))

(define two (add-1 (add-1 zero)))
(define mytwo (lambda (f) (lambda (x) (f (f x)))))

(define (plus m n) (lambda (f) (lambda (x) ((m f) ((n f) x)))))

; tests

((one inc) 0)
((myone inc) 0)

(newline)

((two inc) 0)
((mytwo inc) 0)

(newline)

(((plus one one) inc) 0)
(((plus myone myone) inc) 0)

(newline)

(((plus two two) inc) 0)
(((plus mytwo mytwo) inc) 0)
