#lang racket

(define (myequal? a b)
  (cond ((and (pair? a)
              (pair? b)
              (myequal? (car a) (car b))
              (myequal? (cdr a) (cdr b))) #t)
        ((and (not (pair? a))
              (not (pair? b))
              (eq? a b)) #t)
        (else #f)))

; tests

(myequal? 1 1)
(myequal? 1 2)
(myequal? '() '())
(myequal? '() '(1))
(myequal? '(1) '(1))
(myequal? '(1 1) '(1 2))
(myequal? '(1 2) '(1 2))
(myequal? 1 '(1))