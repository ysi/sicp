#lang lazy

(define (rem a b)
  (display "*")
  (remainder a b))

(define (gcd a b)
  (if (= b 0)
      a
      (gcd b (rem a b))))

(gcd 206 40)
(newline)