#lang racket

(define (last-pair items)
  (list-ref items (- (length items) 1)))

(define (reverse items)
  (define (iter list1 list2)
    (if (null? list1)
        list2
        (iter (cdr list1) (cons (car list1) list2))))
  (iter items '()))

; tests

(reverse (list 1 2 3 4))
