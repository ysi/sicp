#lang racket

(require "common.scm")

; recursive
(define (repeated f n)
  (if (= n 1)
      f
      (compose f (repeated f (- n 1)))))

; iterative
(define (iter-repeated f n)
  (define (iter result count)
    (if (= count 0)
        result
        (iter (compose f result) (- count 1))))
  (iter identity n))

; tests

((repeated square 2) 5)
((iter-repeated square 2) 5)
