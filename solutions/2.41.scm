#lang racket

(require "common.scm")

(define (unique-triplets n)
  (flatmap (lambda (i)
             (flatmap (lambda (j)
                        (map (lambda (k) (list i j k))
                             (enumerate-interval 1 (- j 1))))
                      (enumerate-interval 1 (- i 1))))
           (enumerate-interval 1 n)))

(define (sum-triplet t)
  (+ (car t) (cadr t) (caddr t)))

(define (given-sum-triplets n s)
  (filter
   (lambda (t) (= s (sum-triplet t)))
   (unique-triplets n)))

; tests

(given-sum-triplets 10 24)




