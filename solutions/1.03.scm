#lang racket

(require "common.scm")

(define (sumsq a b) (+ (square a) (square b)))

(define (maxsumsq a b c) 
  (cond ((and (> a c) (> b c)) (sumsq a b))
        ((and (> a b) (> c b)) (sumsq a c))
        (else (sumsq b c))))

; tests

(maxsumsq 2 3 4)
(maxsumsq 2 4 3)
(maxsumsq 3 2 4)
(maxsumsq 3 4 2)
(maxsumsq 4 2 3)
(maxsumsq 4 3 2)

