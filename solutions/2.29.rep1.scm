#lang racket

; first representation

(define (make-mobile left right) (list left right))
(define (make-branch length structure) (list length structure))

(define (left-branch mobile)
  (car mobile))

(define (right-branch mobile)
  (cadr mobile))

(define (branch-length branch)
  (car branch))

(define (branch-structure branch)
  (cadr branch))

(define (total-weight mobile)
  (if (pair? mobile)
      (+ (total-weight (branch-structure (left-branch mobile)))
         (total-weight (branch-structure (right-branch mobile))))
      mobile))


(define (balanced? mobile)
  (if (not (pair? mobile))
      #t
      (let ((lbm (left-branch mobile))
            (rbm (right-branch mobile)))
        (let ((slbm (branch-structure lbm))
              (srbm (branch-structure rbm)))
          (and (= (* (branch-length lbm) (total-weight slbm))
                  (* (branch-length rbm) (total-weight srbm)))
               (balanced? slbm)
               (balanced? srbm))))))
               
(define m (make-mobile 
               (make-branch 1 4) 
               (make-branch 1 
                            (make-mobile 
                             (make-branch 1 2) 
                             (make-branch 1 2)))))

(total-weight m)
(balanced? m)
