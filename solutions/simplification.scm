#lang racket

; https://code.google.com/p/stolzen/source/browse/trunk/sicp/chapter2/4A/mattern-matching.scm
; http://rajiv.sg/blog/2012/07/01/sicp-pattern-matching-and-rule-based-substitution-lecture-with-mit-scheme/

(require rackunit)

(define user-initial-environment (make-base-namespace))

; EXPRESSIONS

; (not (atom? exp)) MEANS (pair? exp) and allows (car exp) and (cdr exp)
(define (atom? exp) (not (pair? exp)))
; (nor (pair? x) (null? x))

(define (constant? exp) (number? exp))
(define (variable? exp) (symbol? exp))
;(define (power? exp) (and (pair? exp) (eq? (car exp) 'pow)))
(define (compound? exp) (pair? exp))

; RULES

; a rule has the form '(pattern skeleton) 
(define rule1 '( ((? op) (?c e1) (?c e2))
                 (: (op e1 e2)) ) )

; OK
(define (pattern rule) (car  rule))
(define pat1 (pattern rule1))
;(check-equal? pat1 '((? op) (?c e1) (?c e2)))

; OK
; skeleton is the second part of a rule
(define (skeleton rule) (cadr rule))
(define skel1 (skeleton rule1))
;(check-equal? skel1 '(: (op e1 e2)) )


; Skeletons & Evaluations

; skeleton has the form '(: exp)
; where exp is skeleton expression
; OK
(define (skeleton? skel)
  (and (pair? skel) (eq? (car skel) ':)))
;(check-true (skeleton? skel1))
; OK
(define (eval-exp skel) (cadr skel))
;(check-equal? (eval-exp skel1) '(op e1 e2))

; PATTERNS

; three types of elementary patterns
; (?c c) matches a constant, calls it c
; (?v v) matches a symbolic variable, calls it v
; (? e) matches an expression, calls it e

(define (arbitrary-constant? pattern) (and (pair? pattern) (eq? (car pattern) '?c)))
(define (arbitrary-variable? pattern) (and (pair? pattern) (eq? (car pattern) '?v)))
;(define (arbitrary-power? pattern) (and (pair? pattern) (eq? (car pattern) '?p)))
(define (arbitrary-expression? pattern) (and (pair? pattern) (eq? (car pattern) '?)))

(define (variable-name pattern) (cadr pattern))


; DICTIONARY

; data structure for storing (key, value) pairs
; key is some elementary pattern
; value is the matched expression (a constant, a variable, a complex expression)
(define (extend-dictionary pat data dict)
  (let ((name (variable-name pat)))
    (let ((v (assq name dict)))
      ; In Racket: if the key is absent then assq returns #f
      (cond ((not v) (cons (list name data) dict))
            ((eq? (cadr v) data) dict)
            (else 'failed)))))

; What can the dictionary contain?
; constants, variables and expressions
;(check-equal? (extend-dictionary '(?c c) 4 '())
;              '((c 4)))
;(check-equal? (extend-dictionary '(?v v) 'z '())
;              '((v z)))
;(check-equal? (extend-dictionary '(? e1) '(y z) '())
;              '((e1 (y z))))


(define (lookup var dict)
  (let ((v (assq var dict)))
    ; In Racket: if the key is absent then assq returns #f
    (if (not v) var (cadr v))))
;(check-equal? (assq 'x '((x 10))) '(x 10))
;(check-equal? (assq 'x '((y 10) (x 10))) '(x 10))
;(check-equal? (assq 'z '((y 10) (x 10))) false)
;(check-equal? (lookup 'x '((x 10))) 10)
;(check-equal? (lookup 'y '((x 10))) 'y)


; expression of the skeleton
; exp = (eval-exp skel)
; evaluate the expression using dict
(define (evaluate exp dict)
  (if (atom? exp)
      (lookup exp dict)
      (apply
       (eval (lookup (car exp) dict)
             user-initial-environment)
       (map (lambda (v) (lookup v dict))
            (cdr exp)))))

;(check-equal? (evaluate 'x '((x 5))) 5)
;(check-equal? (evaluate 'x '((x y))) 'y)
;(check-equal? (evaluate 'x '((x (y z)))) '(y z))
;
;(check-equal? (evaluate '(+ x y) '((x 2) (y 3))) 5)

; recursive tree walk of the skeleton
(define (instantiate skel dict)
  (define (loop s)
    (cond ((atom? s) s)
          ((skeleton? s) (evaluate (eval-exp s) dict))
          ; some pair, not skeleton
          ; instantiate its parts
          (else (cons (loop (car s))
                      (loop (cdr s))))))
  (loop skel))
;(check-equal? 5 (instantiate '(: (+ e1 e2)) '((e1 2) (e2 3))))


; MATCHER

(define (match pat exp dict)
  (cond ((eq? dict 'failed) 'failed)
        ; atomic pattern and expression should be equal
        ; then dictionary is not extended
        ((and (atom? pat) (atom? exp) (eq? pat exp)) dict)
        ; if some pattern matched, then add it and matched value to the dictionary
        ((or
          (and (arbitrary-constant? pat) (constant? exp))
          (and (arbitrary-variable? pat) (variable? exp))
          ;(and (arbitrary-power? pat) (power? exp))
          (arbitrary-expression? pat))
         (extend-dictionary pat exp dict))
        ; complex pattern and expression, then recursive matching
        ((nor (atom? pat) (atom? exp)) (match (cdr pat) (cdr exp) (match (car pat) (car exp) dict)))
        ; anything else is no-match!
        (else 'failed)))
;(match 5 5 '())
;(match 'x 'x '())
;(match '+ '+ '())
;(match '() '() '())
;(match #t #t '())
;(match '(?c c) 5 '())
;(match '(?c c) 'x '())
;(match '(?c c) 'c '())
;(match '(?c c) '() '())
;(match '(?v v) 5 '())
;(match '(?v v) 'x '())
;(match '(?v v) 'v '())
;(match '(?v v) '() '())
;(match '(? e) 5 '())
;(match '(? e) 'x '())
;(match '(? e) 'e '())
;(match '(? e) '() '())
;(match '(? e) '(x) '())
;(match '(? e) '(+) '())


; SIMPLIFIER

; Returns a procedure for the given set of rules
(define (simplifier the-rules)
  ;  (define (simplify-exp exp)
  ;    (try-rules (if (compound? exp)
  ;                   (simplify-parts exp)
  ;                   exp)))
  ;  (define (simplify-parts exp)
  ;    (if (null? exp)
  ;        '()
  ;        (cons (simplify-exp (car exp))
  ;              (simplify-exp (cdr exp)))))
  (define (simplify-exp exp)
    (try-rules
     (if (compound? exp)
         (map simplify-exp exp)
         exp
         )))
  (define (try-rules exp)
    (define (scan rules)
      (if (null? rules)
          exp
          (let ((dict
                 (match (pattern (car rules)) exp '())))
            (if (eq? dict 'failed)
                (scan (cdr rules))
                (simplify-exp
                 (instantiate
                     (skeleton (car rules))
                   dict))))))
    (scan the-rules))
  simplify-exp)

;(define my (simplifier
; '(
;   ( (dd (?c c) (? v)) 0 )
;   ( (dd (?v v) (? v)) 1 )
;   ( (dd (?v u) (? v)) 0 )
;   )))
;
;(my '(dd 5 x))
;(my '(dd x x))
;(my '(dd y x))







(define deriv-rules
  '(
    ( (dd (?c c) (? v)) 0 )
    ( (dd (?v v) (? v)) 1 )
    ( (dd (?v u) (? v)) 0 )
    
    ( (dd (+ (? x1) (? x2)) (? v))
      (+ (dd (: x1) (: v))
         (dd (: x2) (: v))))
    
    ( (dd (* (? x1) (? x2)) (? v))
      (+ (* (: x1) (dd (: x2) (: v)))
         (* (dd (: x1) (: v)) (: x2))))
    
    ( (dd (** (? x) (?c n)) (? v))
      (* (* (: n)
            (** (: x) (: (- n 1))))
         (dd (: x) (: v))))
    
    ))

;(define dsimp (simplifier deriv-rules))

;(define algebra-rules
;  '(
;    

;    ( (* (?c e1) (* (?c e2) (? e3)))
;      (* (: (* e1 e2)) (: e3)))
;    
;    ( (* (? e1) (* (?c e2) (? e3)))
;      (* (: e2) (* (: e1) (: e3))))
;    
;    ( (* (* (? e1) (? e2)) (? e3))
;      (* (: e1) (* (: e2) (: e3))))
;    
;    ( (+ (?c e1) (+ (?c e2) (? e3)))
;      (+ (: (+ e1 e2)) (: e3)))
;    
;    ( (+ (? e1) (+ (?c e2) (? e3)))
;      (+ (: e2) (+ (: e1) (: e3))))
;    
;    ( (+ (+ (? e1) (? e2)) (? e3))
;      (+ (: e1) (+ (: e2) (: e3))))



;    )
;  )

; Tests
; INTERESTING STUFF

(define arithmetic-rules
  '(
    ; summation
    ( (+ 0 (? e)) (: e))
    ( (+ (? e) 0) (: e))
    ( (+ (?c e1) (?c e2)) (: (+ e1 e2)) )
    ( (+ (?v v) (?c c)) (+ (: c) (: v)))
    ( (+ (? e) (? e)) (* 2 (: e)))
    
    ; multiplication
    ( (* 1 (? e)) (: e))
    ( (* (? e) 1) (: e))
    ( (* 0 (? e)) 0)
    ( (* (? e) 0) 0)
    ( (* (?c e1) (?c e2)) (: (* e1 e2)) )
    ( (* (? e) (?c c)) (* (: c) (: e)))
    ( (* (?c a) (* (?c b) (? e))) (* (: (* a b)) (: e)))
    ( (* (* (?c c) (? e1)) (? e2)) (* (: c) (* (: e1) (: e2))))
    ( (* (* (? e1) (?c c)) (? e2)) (* (: c) (* (: e1) (: e2))))
    ( (* (? e1) (* (?c c) (? e2))) (* (: c) (* (: e1) (: e2))))
    
    ; subtraction
    ( (- (? e) 0) (: e))
    ( (- (?c e1) (?c e2)) (: (- e1 e2)) )
    ( (- (? e1) (- (? e2))) (+ (: e1) (: e2)))
    ( (- (? e1) (? e1)) 0)
    
    ; division
    ( (/ (? e) 1) (: e))
    ( (/ 0 (? e)) 0)
    ( (/ (?c e1) (?c e2)) (: (/ e1 e2)) )
    ( (/ (? e) (?c c)) (* (/ 1.0 c) (: e)) )
    
    ; distributive
    ( (+ (* (?c a) (? e)) (* (?c b) (? e)))
      (* (: (+ a b)) (: e)))
    
    ( (- (* (?c a) (? e)) (* (?c b) (? e)))
      (* (: (- a b)) (: e)))
    
    ( (+ (* (? e) (?c a) ) (* (? e) (?c b) ))
      (* (: (+ a b)) (: e)))
    
    ( (- (* (? e) (?c a) ) (* (? e) (?c b) ))
      (* (: (- a b)) (: e)))
    
    ( (* (? e) (+ (? a) (? b)))
      (+ (* (: e) (: a)) (* (: e) (: b))))
    
    ( (*  (+ (? a) (? b)) (? e))
      (+ (* (: a) (: e)) (* (: b) (: e))))

    ; open parenthesis

    ( (* (? c) (+ (? d) (? e))) (+ (* (: c) (: d)) (* (: c) (: e))) )
    ( (* (? c) (- (? d) (? e))) (- (* (: c) (: d)) (* (: c) (: e))) )

    ( (* (+ (? d) (? e)) (? c)) (+ (* (: d) (: c)) (* (: e) (: c))) )
    ( (* (- (? d) (? e)) (? c)) (- (* (: d) (: c)) (* (: e) (: c))) )

    
    ))

;(define arithmetic-simplifier (simplifier arithmetic-rules))

(define deriv-simplifier (simplifier deriv-rules))
(define arithmetic-simplifier (simplifier arithmetic-rules))
(define full-deriv-simplifier (simplifier (append deriv-rules arithmetic-rules)))

;(deriv-simplifier '(dd (* (* x y) (+ x 3)) x))
;(full-deriv-simplifier '(dd (* (* x y) (+ x 3)) x))
;(newline)
;(deriv-simplifier '(dd (* (* (* 2 x) (+ x y)) (* (* 2 x) (+ x y))) x))

;(full-deriv-simplifier '(dd (* (* (* 2 x) (+ x y)) (* (* 2 x) (+ x y))) x))
;(arithmetic-simplifier '(* y (* 2 z)))

;(arithmetic-simplifier '(* (* (+ a 1) (+ a 2)) (* (+ 2 a) (+ 1 a))))

;(arithmetic-simplifier '(* (- q 1) (+ (* q q) (+ q 1))))

;(arithmetic-simplifier '(* (+ a b) (+ a b)) )


                  