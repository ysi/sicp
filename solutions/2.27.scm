#lang racket

(define (deep-reverse items)
  (cond ((null? items) items)
        ((pair? items) (map deep-reverse (reverse items)))
        (else items)))
  
; tests

(define x (list (list 1 2) (list 3 4)))
x
(reverse x)
(deep-reverse x)
(newline)
(define y (list (list 1 (list 2 3)) (list 4 (list 5 6))))
y
(reverse y)
(deep-reverse y)
