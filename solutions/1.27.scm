#lang racket

(require "common.scm")

(define (next n) (if (= n 2) 3 (+ n 2)))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next test-divisor)))))

(define (smallest-divisor n)
  (find-divisor n 2))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- (remainder n 4294967087) 1)))))

(define (is-carmichael? n)
  (define (iter a)
    (if (= a 0)
        true
        (if (= (expmod a n n) a)
            (iter (- a 1))
            false)))
  (and (iter (- n 1)) (not (prime? n))))

(define (display-carmichaels a b)
  (cond ((is-carmichael? a) (display a) (newline)))
  (cond ((<= a b) (display-carmichaels (+ a 1) b))))

; tests

(display-carmichaels 500 3000)
