#lang racket

(require "common.scm")

(define (double f)
  (lambda (x) (f (f x))))

(inc 0) ; inc^1
((double inc) 0) ; inc^2
((double (double inc)) 0) ; (inc^2)^2 = inc^4
(((double double) inc) 0) ; inc^(2^2) = inc^4
(((double double) (double inc)) 0) ; (inc^2)^(2^2) = inc^8
((((double double) double) inc) 0) ; inc^(2^(2^2)) = inc^16
(((double (double double)) inc) 0) ; inc^((2^2)^2) = inc^16
