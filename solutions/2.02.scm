#lang racket

(require "common.scm")

(provide x-point)
(provide y-point)
(provide make-point)
(provide start-segment)
(provide end-segment)
(provide make-segment)

(define x-point car)
(define y-point cdr)
(define make-point cons)

(define start-segment car)
(define end-segment cdr)
(define make-segment cons)

(define (midpoint-segment segment)
  (make-segment
   (average 
    (x-point (start-segment segment)) 
    (x-point (end-segment segment)))
   (average 
    (y-point (start-segment segment)) 
    (y-point (end-segment segment)))))
   
; tests

(define s (make-segment (make-point 2 4) (make-point 10 10)))

(x-point s)
(y-point s)
(midpoint-segment s)

