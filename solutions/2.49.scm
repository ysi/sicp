#lang racket

(require "picture.language.scm")
(require "picture.window.scm")

; tests

((below
  (beside diamond wave)
  (beside outline x-painter))
 unit-frame)
