#lang racket

;; Fixed points

(define tolerance 0.00001)
(define dx tolerance)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (average x y)
  (/ (+ x y) 2))

(define (square x) (* x x))

(define (average-damp f)
  (lambda (x) (average x (f x))))

(define (deriv g)
  (lambda (x) (/ (- (g (+ x dx)) (g x)) dx )))

(define (newton-transform g)
  (lambda (x) (- x (/ (g x) ((deriv g) x)))))

(define (newtons-method g guess)
  (fixed-point (newton-transform g) guess))


(define (sqrt x)
  (newtons-method
   (lambda (y) (- (square y) x)) 1.0))

; the returned result is a fixed point of the transformed function
(define (fixed-point-of-transform g transform guess)
  (fixed-point (transform g) guess))

(define (tsqrt x)
  (fixed-point-of-transform
   (lambda (y) (/ x y))
   average-damp
   1.0))

(define (nsqrt x)
  (fixed-point-of-transform
   (lambda (y) (- (square y) x)) newton-transform 1.0))


  



;(define (cube x) (* x x x))
;((deriv cube) 5)
(sqrt 2)
(tsqrt 2)
(nsqrt 2)