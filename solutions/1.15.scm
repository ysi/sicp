#lang racket

(require "common.scm")

(define (p x)
  (- (* 3 x) (* 4 (cube x))))

(define (sine angle)
  (if (not (> (abs angle) 0.1))
      angle
      (p (sine (/ angle 3.0)))))

(define (sine1 angle)
  (cond
    ((not (> (abs angle) 0.1)) angle)
    (else
     (display "angle=")
     (display (/ angle 3.0))
     (newline)
     (p (sine1 (/ angle 3.0))))))

; tests

(sine 12.15)
(newline)
(sine1 12.15)
