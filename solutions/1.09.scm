#lang racket

(require "common.scm")

(define (rec-add a b)
  (if (= a 0)
      b
      (inc (rec-add (dec a) b))))

(define (iter-add a b)
  (if (= a 0)
      b
      (iter-add (dec a) (inc b))))

; tests

; linear recursion

(rec-add 4 5)
;(inc (rec-add 3 5))
;(inc (inc (rec-add 2 5)))
;(inc (inc (inc (rec-add 1 5))))
;(inc (inc (inc (inc (rec-add 0 5)))))
;(inc (inc (inc (inc 5))))
;(inc (inc (inc 6)))
;(inc (inc 7))
;(inc 8)
;9

; linear iteration

(iter-add 4 5)
;(iter-add 3 6)
;(iter-add 2 7)
;(iter-addd 1 8)
;(iter-add 0 9)
;9
