#lang racket

(define (cons x y)
  (lambda (m) (m (* (expt 2 x) (expt 3 y)))))

(define (car z)
  (z (lambda (r) 
       (define (iter r res)
         (if (even? r) 
             (iter (quotient r 2) (+ res 1))
             res))
       (iter r 0))))
 
(define (cdr z)
  (z (lambda (r) 
       (define (iter r res)
         (if (and (> r 1) (= 0 (remainder r 3)))
             (iter (quotient r 3) (+ res 1))
             res))
       (iter r 0))))

; tests

(define a (cons 10 20))
(car a)
(cdr a)