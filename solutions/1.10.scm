#lang racket

(define (A x y)
  (cond ((= y 0) 0)
        ((= x 0) (* 2 y))
        ((= y 1) 2)
        (else (A (- x 1)
                 (A x (- y 1))))))


(define (f n) (A 0 n))
; 2n
(define n1 17)
(f n1)
(* 2 n1)

(newline)

; (A 1 10)
; 1024

(define (g n) (A 1 n))
; 2^n
(define n2 12)
(g n2)
(expt 2 n2)

(newline)

; (A 2 4)
; 65536

; 2^(2^(2^2)) = 2^16
(define (h n) (A 2 n))
; 2^2^...^2 (n times)
(h 4)
(expt 2 (expt 2 (expt 2 2)))

(newline)

; (A 3 3)
; (A 2 (A 3 2))
; (A 2 (A 2 (A 3 1)))
; (A 2 (A 2 2))
; (A 2 4)
; 65536 = 2^16

