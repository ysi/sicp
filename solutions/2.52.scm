#lang racket

(require "picture.language.scm")
(require "picture.window.scm")

(define (new-wave frame)
  ((segments->painter
    (append
     (connect (list (make-vect 0.41  0.01)
                    (make-vect 0.51  0.32)
                    (make-vect 0.61  0.01))) ; inside legs
     
     (connect (list (make-vect 0.24 0.01)
                    (make-vect 0.43 0.54)
                    (make-vect 0.31  0.56)
                    (make-vect 0.11  0.51)
                    (make-vect 0.01  0.64))) ; lower left
     
     (connect (list (make-vect 0.01  0.73)
                    (make-vect 0.11  0.61)
                    (make-vect 0.32 0.67)
                    (make-vect 0.46  0.64)
                    (make-vect 0.41 0.81)
                    (make-vect 0.44  1.0))) ; upper left
     
     (connect (list (make-vect 0.74 0.02)
                    (make-vect 0.59  0.54)
                    (make-vect 0.64  0.56)
                    (make-vect 1.0  0.27))) ; lower right
     
     (connect (list (make-vect 1.0  0.36)
                    (make-vect 0.67  0.67)
                    (make-vect 0.55  0.66)
                    (make-vect 0.61 0.81)
                    (make-vect 0.54  1.0)
                    (make-vect 0.44  1.0))) ; upper right
     
     (connect (list (make-vect 0.47  0.7)
                    (make-vect 0.5  0.69)
                    (make-vect 0.53  0.7)
                    )))) ; smile
   frame))

(define (new-corner-split painter n)
  (if (= n 0)
      painter
      (let ((up (up-split painter (- n 1)))
            (right (right-split painter (- n 1))))
        (let ((top-left (beside empty-painter up))
              (bottom-right (below empty-painter right))
              (corner (new-corner-split painter (- n 1))))
          (beside (below painter top-left)
                  (below bottom-right corner))))))

(define (new-square-limit painter n)
  ((square-of-four rotate270 rotate180 identity rotate90)
   (new-corner-split painter n)))

; tests

;((add-painters wave new-wave) unit-frame)
;((new-corner-split wave 4) unit-frame)
;((new-square-limit new-wave 4) unit-frame)