#lang racket

(define (last-pair items)
  (list-ref items (- (length items) 1)))

; tests

(last-pair (list 1 2 3 4))

