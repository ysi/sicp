#lang racket

(provide div-interval)

(define make-interval cons)
(define lower-bound car)
(define upper-bound cdr)

(define (mul-interval x y)
  (let ((p1 (* (lower-bound x) (lower-bound y)))
        (p2 (* (lower-bound x) (upper-bound y)))
        (p3 (* (upper-bound x) (lower-bound y)))
        (p4 (* (upper-bound x) (upper-bound y))))
    (make-interval (min p1 p2 p3 p4)
                   (max p1 p2 p3 p4))))

(define (div-interval x y)
  (define y1 (lower-bound y))
  (define y2 (upper-bound y))
  (if (and (<= y1 0) (>= y2 0))
      (error "division by interval with zero")
      (mul-interval
       x 
       (make-interval (/ 1.0 y2)
                      (/ 1.0 y1)))))

; tests

(require rackunit)

(check-equal?
 (div-interval (make-interval 8 10) (make-interval 2 4))
 (make-interval 2.0 5.0))

(check-exn
 exn:fail?
 (lambda ()
   (div-interval (make-interval 8 10) (make-interval -8 8))))
