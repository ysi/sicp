# iterative counting change
# optimal algorithm with finding every variant of change
# but how to implement these nested loops in scheme?

# amount
a = 100

# denominations
m = [50,25,10,5,1]

# number of calls
calls = 0

def iter_change(a):
	global calls
	count = 0
	for i0 in range(a//m[0] + 1):
		for i1 in range((a - i0*m[0])//m[1] + 1):
			for i2 in range((a - (i0*m[0] + i1*m[1]))//m[2] + 1):
				for i3 in range((a - (i0*m[0] + i1*m[1] + i2*m[2]))//m[3] + 1):
					if (a - (i0*m[0] + i1*m[1] + i2*m[2] + i3*m[3])) % m[4] == 0:
						count = count + 1
					calls = calls + 1
	return count

print "amount = ", a, "  ways of change = ", iter_change(a), "  calls = ", calls
#number of calls ~ amount^(n-1), where n=len(m)
