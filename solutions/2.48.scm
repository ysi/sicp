#lang racket

(require "picture.language.scm")

; Tests

(define s
  (make-segment (make-vect 1 2) (make-vect 3 4)))

s
(start-segment s)
(end-segment s)
