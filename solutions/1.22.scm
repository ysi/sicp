#lang racket

(require "common.scm")

(define (smallest-divisor n)
  (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (+ test-divisor 1)))))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))
      (display "")))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (search-for-primes a b) 
  (timed-prime-test a)
  (cond ((< a b) (search-for-primes (+ a 1) b))))

; tests

;(search-for-primes 1000 1019)
;(search-for-primes 10000 10037)
;(search-for-primes 100000 100043)
;(search-for-primes 1000000 1000037)

;(define x 10000000000000000)
;(search-for-primes x (+ x 100))

(timed-prime-test 10000000000000061)
(newline)

; The data support Theta(sqrt(n)) prediction,
; and programs on the machine run in time 
; proportional to the number of computation steps
