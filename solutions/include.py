#!/usr/bin/env python

# Input: argv[1] - text file to read
# Output: stdout
# "IncludeFile <filename>" is replaced by <filename> content.

import sys

if len(sys.argv)<2:
  print "Not enough arguments"
  exit()

f = open(sys.argv[1], 'r')
for line in f:
  if line[:11]=="IncludeFile":
    ifile=line[12:-1]
    f1 = open(ifile, 'r')
    for line1 in f1:
      print line1[:-1]
    f1.close
  else:
    print line[:-1]
f.close()
