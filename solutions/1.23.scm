#lang racket

(require "common.scm")

(define (next n) (if (= n 2) 3 (+ n 2)))

(define (smallest-divisor n)
  (find-divisor n 2))

(define (find-divisor n test-divisor)
  (cond ((> (square test-divisor) n) n)
        ((divides? test-divisor n) test-divisor)
        (else (find-divisor n (next test-divisor)))))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (timed-prime-test n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime n (- (runtime) start-time))
      (display "")))

(define (report-prime p elapsed-time)
  (display p)
  (display " *** ")
  (display elapsed-time)
  (newline))

(define (search-for-primes a b) 
  (timed-prime-test a)
  (cond ((< a b) (search-for-primes (+ a 1) b))))

(define (search-for-primes-start n)
  (search-for-primes (+ n 1) (+ n 1000)))

; tests

(timed-prime-test 10000000000000061)
(newline)

; the ratio is slightly less than 2
