denom = [1,5,10,25,50] # best (and correct!) results when sorted from low to high!


class MyDenom:
    depth = 0
    maxDepth = 0
    calls = 0

    def cc(self, amount, kinds):
        #global depth, maxDepth, calls
        MyDenom.depth += 1
        if MyDenom.maxDepth < MyDenom.depth: MyDenom.maxDepth = MyDenom.depth
        MyDenom.calls += 1
        if (amount==0):
            result = 1
        else:
            if ((amount<0) or (kinds==0)):
                result = 0
            else:
                result = MyDenom.cc(self, amount, kinds-1) + MyDenom.cc(self, amount - denom[kinds-1], kinds)
        MyDenom.depth -= 1
        return result

    def exp(self, amount):
        MyDenom.depth = 0
        MyDenom.maxDepth = 0
        MyDenom.calls = 0
        num = MyDenom.cc(self, amount, 5)
        return num, MyDenom.maxDepth, MyDenom.calls

a = MyDenom()

import math

for amount in range(100, 320, 21):

    num, dep, call = a.exp(amount)
    print (amount, call/4**(amount/100))
