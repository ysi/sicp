#lang racket

;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; two-dimensional vectors
;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; constructor

(define (make-vect x y) 
  (cons x y))
(provide make-vect)

; selectors

(define (xcor-vect vect)
  (car vect))
(provide xcor-vect)

(define (ycor-vect vect)
  (cdr vect))
(provide ycor-vect)

; vector methods

(define (add-vect v1 v2)
  (make-vect 
   (+ (xcor-vect v1) (xcor-vect v2))
   (+ (ycor-vect v1) (ycor-vect v2))))
(provide add-vect)

(define (sub-vect v1 v2)
  (make-vect 
   (- (xcor-vect v1) (xcor-vect v2))
   (- (ycor-vect v1) (ycor-vect v2))))
(provide sub-vect)

(define (scale-vect s v)
  (make-vect 
   (* s (xcor-vect v))
   (* s (ycor-vect v))))
(provide scale-vect)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;     Frames
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (make-frame origin edge1 edge2)
  (cons origin (cons edge1 edge2)))
(provide make-frame)

(define (origin-frame frame)
  (car frame))
(provide origin-frame)

(define (edge1-frame frame)
  (cadr frame))
(provide edge1-frame)

(define (edge2-frame frame)
  (cddr frame))
(provide edge2-frame)

(define (frame-coord-map frame)
  (lambda (v) 
    (add-vect
     (origin-frame frame)
     (add-vect 
      (scale-vect (xcor-vect v) (edge1-frame frame))
      (scale-vect (ycor-vect v) (edge2-frame frame))))))
(provide frame-coord-map)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;     Segments
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (make-segment start-vector end-vector)
  (cons start-vector end-vector))
(provide make-segment)

(define (start-segment segment)
  (car segment))
(provide start-segment)

(define (end-segment segment)
  (cdr segment))
(provide end-segment)

;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Transforms
;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (transform-painter painter origin corner1 corner2)
  (lambda (frame)
    (let ((m (frame-coord-map frame)))
      (let ((new-origin (m origin)))
        (painter
         (make-frame
          new-origin
          (sub-vect (m corner1) new-origin)
          (sub-vect (m corner2) new-origin)))))))

(define (flip-vert painter)
  (transform-painter
   painter
   (make-vect 0.0 1.0)    ; new origin
   (make-vect 1.0 1.0)    ; new end of edge1
   (make-vect 0.0 0.0)))  ; new end of edge2
(provide flip-vert)

(define (flip-horiz painter)
  (transform-painter
   painter
   (make-vect 1.0 0.0)    ; new origin
   (make-vect 0.0 0.0)    ; new end of edge1
   (make-vect 1.0 1.0)))  ; new end of edge2
(provide flip-horiz)

(define (shrink-to-upper-right painter)
  (transform-painter
   painter
   (make-vect 0.5 0.5)
   (make-vect 1.0 0.5)
   (make-vect 0.5 1.0)))
(provide shrink-to-upper-right)

(define (rotate90 painter)
  (transform-painter
   painter
   (make-vect 1.0 0.0)
   (make-vect 1.0 1.0)
   (make-vect 0.0 0.0)))
(provide rotate90)

(define (rotate180 painter)
  (transform-painter
   painter
   (make-vect 1.0 1.0)
   (make-vect 0.0 1.0)
   (make-vect 1.0 0.0)))
(provide rotate180)

(define (rotate270 painter)
  (transform-painter
   painter
   (make-vect 0.0 1.0)
   (make-vect 0.0 0.0)
   (make-vect 1.0 1.0)))
(provide rotate270)

(define (squash-inwards painter)
  (transform-painter
   painter
   (make-vect 0.0 0.0)
   (make-vect 0.65 0.35)
   (make-vect 0.35 0.65)))
(provide squash-inwards)

(define (beside painter1 painter2)
  (let ((split-point (make-vect 0.5 0.0)))
    (lambda (frame)
      ((transform-painter
        painter1
        (make-vect 0.0 0.0)
        split-point
        (make-vect 0.0 1.0))
       frame)
      ((transform-painter
        painter2
        split-point
        (make-vect 1.0 0.0)
        (make-vect 0.5 1.0))
       frame))))
(provide beside)

(define (below painter1 painter2)
  (let ((split-point (make-vect 0.0 0.5)))
    (lambda (frame)
      ((transform-painter
        painter1
        (make-vect 0.0 0.0)
        (make-vect 1.0 0.0)
        split-point)
       frame)
      ((transform-painter
        painter2
        split-point
        (make-vect 1.0 0.5)
        (make-vect 0.0 1.0))
       frame))))
(provide below)

(define (split operation1 operation2)
  (define (iter painter n)
    (if (= n 0)
        painter
        (let ((smaller (iter painter (- n 1))))
          (operation1 painter (operation2 smaller smaller)))))
  (lambda (painter n) (iter painter n)))
(provide split)

(define right-split (split beside below))
(provide right-split)

(define up-split (split below beside))
(provide up-split)

(define (square-of-four tl tr bl br)
  (lambda (painter)
    (let ((top (beside (tl painter) (tr painter)))
          (bottom (beside (bl painter) (br painter))))
      (below bottom top))))
(provide square-of-four)

(define (corner-split painter n)
  (if (= n 0)
      painter
      (let ((up (up-split painter (- n 1)))
            (right (right-split painter (- n 1))))
        (let ((top-left (beside up up))
              (bottom-right (below right right))
              (corner (corner-split painter (- n 1))))
          (beside (below painter top-left)
                  (below bottom-right corner))))))
(provide corner-split)

(define (square-limit painter n)
  ((square-of-four flip-horiz identity rotate180 flip-vert)
   (corner-split painter n)))
(provide square-limit)

(define (add-painters painter . painters)
  (lambda (frame)
    (map (lambda (painter) (painter frame)) painters)
    (painter frame)))
(provide add-painters)

;(define flipped-pairs
;  (square-of-four identity flip-vert identity flip-vert))