#lang racket

(require "common.scm")

(define tolerance 0.00001)
(define dx tolerance)

(define (repeated f n)
  (if (= n 0)
      identity
      (compose f (repeated f (- n 1)))))

(define (testfun x n)
  (lambda (y) (/ x (expt y (- n 1)))))

(define (root n x)
  (fixed-point ((repeated average-damp (- n 1)) (testfun x n)) 1.0))


; this is cool!
(define (iterative-improvement good-enough? improve)
  (lambda (guess)
    (define (iter guess)
      (if (good-enough? guess)
        guess
        (iter (improve guess))))
    (iter guess)))

(define (sqrt x)
  (define (good-enough? guess) (< (abs (- (square guess) x)) 0.001))
  (define (improve guess) (average guess (/ x guess)))
  ((iterative-improvement good-enough? improve) 1.0))

(define (close-enough? v1 v2)
  (< (abs (- v1 v2)) tolerance))

(define (fixed-point f first-guess)
  (define (good-enough? guess) (close-enough? guess (f guess)))
  (define (improve guess) (f guess))
  ((iterative-improvement good-enough? improve) first-guess))

(define (fsqrt x)
  (fixed-point (average-damp (lambda (y) (/ x y))) 1.0))

(define phi (fixed-point (lambda (x) (+ 1 (/ 1.0 x))) 1.0))

; tests

(sqrt 2)
(fsqrt 2)
phi

