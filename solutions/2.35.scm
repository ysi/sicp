#lang racket

(require "common.scm")

(define (count-leaves t)
  (accumulate
   +
   0
   (map 
    (lambda (x) (if (pair? x) 0 1))
    (enumerate-tree t)
    )))

; tests

(count-leaves (list (list 1 2 3) (list 4 5 6) 7))
