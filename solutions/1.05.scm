#lang racket

(define (p) (p))
(define (test x y) (if (= x 0) 0 (p)))

; tests

; process doesn't stop for applicative-order evaluation
; (test 0 (p))
