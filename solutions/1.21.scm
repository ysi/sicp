#lang racket

(require "common.scm")

(define (smallest-divisor n)
  (define (find-divisor test-divisor) 
    (cond ((> (square test-divisor) n) n)
          ((divides? test-divisor n) test-divisor)
          (else (find-divisor (+ test-divisor 1)))))
  (find-divisor 2))

; tests

(smallest-divisor 199)
(smallest-divisor 1999)
(smallest-divisor 19999)

