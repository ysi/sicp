#lang racket

(require "common.scm")

(define (new-if predicate then-clause else-clause)
  (cond (predicate then-clause)
        (else else-clause)))

(define (improve guess x)
  (average guess (/ x guess)))

(define (good-enough? guess x)
  (< (abs (- (square guess) x)) 0.001))

(define (sqrt x)
  (sqrt-iter 1.0 x))

(define (sqrt-iter guess x)
  (new-if (good-enough? guess x)
          guess
          ; the last argument
          (sqrt-iter (improve guess x)
                     x)))

; tests

(new-if (= 1 1) #t #f)
(new-if (= 2 3) #t #f)

;(sqrt 2)
; process stops with "out of memory" error
