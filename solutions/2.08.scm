#lang racket

(require "2.07.scm")

(define (sub-interval x y)
  (make-interval
   (- (lower-bound x) (upper-bound y))
   (- (upper-bound x) (lower-bound y))))

; tests

(sub-interval
 (make-interval -2 2)
 (make-interval -3 3))

(add-interval
 (make-interval -2 2)
 (make-interval -3 3))
