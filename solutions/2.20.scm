#lang racket

(define (same-parity first . items)
  (define (rec items)
    (if (null? items)
        '()
        (if (equal? (even? first) (even? (car items)))
            (cons (car items) (rec (cdr items)))
            (rec (cdr items)))))
  (cons first (rec items)))

(define (iter-same-parity first . items)
  (define (iter items result)
    (if (null? items) 
        result
        (iter
         (cdr items)
         (if (equal? (even? first) (even? (car items)))
             (cons (car items) result)
             result))))
  (reverse (iter items (list first))))

; tests

(same-parity 1 2 3 4 5 6 7)
(same-parity 2 3 4 5 6 7)
(iter-same-parity 1 2 3 4 5 6 7)
(iter-same-parity 2 3 4 5 6 7)
