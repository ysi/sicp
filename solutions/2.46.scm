#lang racket

(require "picture.language.scm")

; tests

(define v1 (make-vect 1 2))
(define v2 (make-vect 3 4))

(xcor-vect v1)
(ycor-vect v1)
(add-vect v1 v2)
(sub-vect v1 v2)
(scale-vect 5 v1)
