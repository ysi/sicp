#lang racket

(require "common.scm")

(define empty-board '())

(define (adjoin-position row col positions)
  (cons (cons row col) positions))

(define (safe? qcol positions)
  (define rest (cdr positions))
  (cond ((empty? rest) #t)
        (else
         (define position (car positions))
         (define drow (abs (- (car position) (caar rest))))
         (and (nor (= drow 0) (= drow (abs (- qcol (cdar rest)))))
              (safe? qcol (cons position (cdr rest)))))))

(define (queens board-size)
  (define (queen-cols k)
    (if (= k 0)
        (list empty-board)
        (filter
         (lambda (positions) (safe? k positions))
         (flatmap
          (lambda (rest-of-queens)
            (map
             (lambda (new-row)
               (adjoin-position new-row k rest-of-queens))
             (enumerate-interval 1 board-size)))
          (queen-cols (- k 1))))))
  (queen-cols board-size))

; tests

(define start-time (current-inexact-milliseconds))
(define result (queens 8))
(define elapsed-time (/ (- (current-inexact-milliseconds) start-time) 1000))
(display elapsed-time) 
(display " seconds")
(newline)
(length result)
;(map (lambda (positions) 
;       (map (lambda (position)
;              (car position)) positions))
;     result)
