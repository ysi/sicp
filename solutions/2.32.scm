#lang racket

(define (subsets s)
  (if (null? s)
      (list '())
      (let ((rest (subsets (cdr s))))
        (append rest (map 
                      (lambda (t) (cons (car s) t))
                      rest)))))

(define (subsets1 s)
  (if (null? s)
      (list '())
      (let ((rest (subsets1 (cdr s))))
        (append rest (map 
                      (lambda (t) (append (list (car s)) t))
                      rest)))))

; tests

(subsets (list 1 2 3))
(subsets1 (list 1 2 3))
