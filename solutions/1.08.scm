#lang racket

; the version with relative error check

(require "common.scm")

(define (improve guess x)
  (/ (+ (/ x (square guess)) 
        (* 2 guess))
     3))

(define (good-enough? oldguess guess)
  (< (/ (abs (- oldguess guess)) guess) 1e-10))

(define (sqrt3-iter oldguess guess x) 
  (if (good-enough? oldguess guess)
      guess
      (sqrt3-iter guess (improve guess x) x)))

(define (mysqrt3 x) (sqrt3-iter 2.0 1.0 x))

(define (test radicand)
  (define calculated (mysqrt3 radicand))
  (define exact (expt radicand (/ 1 3)))
  (display "radicand: ")
  (display radicand)
  (newline)
  (display "sqrt3 exact: ")
  (display exact)
  (newline)
  (display "sqrt3 calculated: ")
  (display calculated)
  (newline))

; tests

(test 1e80)
(test 1e-80)
