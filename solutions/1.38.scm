#lang racket

(require math/base)

; iterative
(define (iter-cont-frac D N count)
  (define (iter result count)
    (if (= count 0)
        result
        (iter (/ (N count) (+ (D count) result)) (- count 1))))
  (iter 0 count))

(define (N i) 1.0)

(define (D i)
  (if (= 0 (modulo (+ i 1) 3))
      (* 2 (quotient (+ i 1) 3))
      1))

(define (show D count)
  (display count)
  (display " ")
  (display (D count))
  (newline)
  (if (= count 0)
      0
      (show D (- count 1))))

; tests

(show D 11)
(newline)
(+ 2 (iter-cont-frac D N 20))
euler.0 ; exact
