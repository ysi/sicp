#lang racket

(require "common.scm")

(define (horner-eval x coefficient-sequence)
  (accumulate
   (lambda (this-coeff higher-terms) 
     (+ this-coeff (* x higher-terms)))
   0
   coefficient-sequence))

; tests

; (list a0 a1 a2 ...)
(horner-eval 2 (list 1 3 0 5 0 1))


