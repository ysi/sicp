#lang racket

; set as binary tree 2.63

(define (entry tree) (car tree))

(define (left-branch tree) (cadr tree))

(define (right-branch tree) (caddr tree))

(define (make-tree entry left right)
  (list entry left right))

(define (adjoin-set x set)
  (cond ((null? set) (make-tree x '() '()))
        ((= (key x) (key (entry set))) set)
        ((< (key x) (key (entry set)))
         (make-tree (entry set) 
                    (adjoin-set x (left-branch set))
                    (right-branch set)))
        ((> (key x) (key (entry set)))
         (make-tree (entry set)
                    (left-branch set)
                    (adjoin-set x (right-branch set))))))


; information retrieval

(define (lookup k set)
  (cond ((null? set) false)
        ((= k (key (entry set))) (entry set))
        ((< k (key (entry set))) (lookup k (left-branch set)))
        ((> k (key (entry set))) (lookup k (right-branch set)))))

(define (make-element key data)
  (list key data))

(define (key x)
  (car x))

(define (data x)
  (cadr x))

; tests

(define (adjoin-list-set xlist set)
  (define (iter xlist set)
  (if (null? xlist)
      set
      (iter (cdr xlist) (adjoin-set (car xlist) set))))
    (iter (reverse xlist) set))

(define t (adjoin-list-set
           (map
            (lambda (x) (make-element x (list 'info x)))
            '(5 6 1 7 3 4 2))
           '()))

(lookup 0 t)
(lookup 1 t)
(lookup 2 t)
(lookup 3.3 t)
(lookup 5 t)
(lookup 8 t)

