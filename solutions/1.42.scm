#lang racket

(require "common.scm")

((compose square inc) 6) ; (6+1)^2 = 49
((compose inc square) 6) ; 6^2+1 = 37
