#lang racket

(require "common.scm")

(define tolerance 0.00001)
(define dx tolerance)

; recursive
(define (repeated f n)
  (if (= n 0)
      identity
      (compose f (repeated f (- n 1)))))

; iterative
(define (it-repeated f n)
  (define (iter result count)
    (if (= count 0)
        result
        (iter (compose f result) (- count 1))))
  (iter identity n))


(define (deriv g)
  (lambda (x) (/ (- (g (+ x dx)) (g x)) dx )))

(define (average3 a b c)
  (/ (+ a b c) 3))

(define (smooth f)
  (lambda (x) (average3
               (f (- x dx))
               (f x)
               (f (+ x dx)))))

; tests

(define func square)
(define func1 (smooth func))
(define func5 ((repeated smooth 5) func))
(func5 1)
