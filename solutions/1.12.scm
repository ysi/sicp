#lang racket

(define (coeff m n)
  (cond
    ((or (= n 0)
         (= m 0)
         (= m n))
     1)
    (else (+ (coeff (- m 1) (- n 1))
             (coeff m (- n 1))))))

; tests

(coeff 0 0)
(newline)
(coeff 0 1)
(coeff 1 1)
(newline)
(coeff 0 2)
(coeff 1 2)
(coeff 2 2)
(newline)
(coeff 0 3)
(coeff 1 3)
(coeff 2 3)
(coeff 3 3)
(newline)
(coeff 0 4)
(coeff 1 4)
(coeff 2 4)
(coeff 3 4)
(coeff 4 4)
