#lang racket

(require "2.07.scm")

(provide make-center-width)
(provide center)
(provide width)
(provide make-center-percent)
(provide tolerance)

(define (make-center-width c w)
  (make-interval (- c w) (+ c w)))

(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))

(define (width i)
  (/ (- (upper-bound i) (lower-bound i)) 2))

; tol: 0.1 means 10%
(define (make-center-percent c t)
  (make-center-width c (* c t)))
  
(define (tolerance i)
  (define c (center i))
  (if (= c 0)
      (error "Interval with center at zero has no tolerance")
      (/ (width i) c)))


