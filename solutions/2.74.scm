#lang racket

(require "common.scm")

(define (apply-generic op . args)
  (let ((type-tags (map type-tag args)))
    (let ((proc (get op type-tags)))
      (if proc
          (apply proc (map contents args))
          (error
           "No method for these types -- APPLY-GENERIC"
           (list op type-tags))))))

; tags

(define (attach-tag type-tag contents)
  (cons type-tag contents))

(define (type-tag datum)
  (if (pair? datum)
      (car datum)
      (error "Bad tagged datum: TYPE-TAG" datum)))

(define (contents datum)
  (if (pair? datum)
      (cdr datum)
      (error "Bad tagged datum: CONTENTS" datum)))

; unordered set with no duplicates

(define (element-of-set? x set)
  (cond ((null? set) false)
        ((equal? x (car set)) true)
        (else (element-of-set? x (cdr set)))))

(define (adjoin-set x set)
  (if (element-of-set? x set)
      set
      (cons x set)))

; division 1

(define (install-div1-package)
  ;; internal procedures
  
  ;; a key and a value in an entry is a pair
  (define make-entry cons)
  (define get-key car)
  (define get-value cdr)
  
  ;; a record is an unordered set of entries
  (define (make-record id name address salary)
    (adjoin-set (make-entry 'id id)
                (adjoin-set (make-entry 'name name)
                            (adjoin-set (make-entry 'address address)
                                        (adjoin-set (make-entry 'salary salary) '())))))
  
  (define (get-file) 
    (adjoin-set (make-record 1 'JohnSilver '5Ave3 1700)
                (adjoin-set (make-record 2 'MeganFox 'OakStr23 2500)
                            (adjoin-set (make-record 3 'JackSparrow '17Ave73 3000)
                                        (adjoin-set (make-record 7 'JamesBond 'PortoBlvd 2100)
                                                    '())))))
    
  
  ; the search in an unordered set
  (define (get-entry key record)
    (cond ((null? record) false)
          ((equal? key (get-key (car record)))
           (car record))
          (else (get-entry key (cdr record)))))

  ; getters
  (define (get-id record) (get-value (get-entry 'id record)))
  (define (get-name record) (get-value (get-entry 'name record)))
  (define (get-address record) (get-value (get-entry 'address record)))
  (define (get-salary record) (get-value (get-entry 'salary record)))
  
  ; the search in an unordered set
  (define (get-record-by-id id file)
    (cond ((null? file) false)
          ((equal? id (get-id (car file)))
           (car file))
          (else (get-record-by-id id (cdr file)))))
  
  (define (get-record-by-name name file)
    (cond ((null? file) false)
          ((equal? name (get-name (car file)))
           (car file))
          (else (get-record-by-name name (cdr file)))))
    
  ;; interface to the rest of the system
  
  (define (tag x) (attach-tag 'div1 x))
  (put 'get-file 'div1 (tag (get-file)))
  (put 'get-id '(div1) get-id)
  (put 'get-name '(div1) get-name)
  (put 'get-address '(div1) get-address)
  (put 'get-salary '(div1) get-salary)
  (put 'get-record-by-id '(div1 div1) get-record-by-id)
  (put 'get-record-by-name '(div1 div1) get-record-by-name)
  )

; division 2

(define (install-div2-package)
  ;; internal procedures
  
  ;; a record is an unordered set of entries
  (define (make-record id name address salary)
    (cons (cons id name) (cons address salary)))
  
  (define (get-file)
    (list
     (make-record 'TheDoctor 'DoctorWho 'M13 5000)
     (make-record 'AceVentura 'JimCarrey 'PortoBlvd 2700)))

  ; getters
  (define (get-id record) (caar record))
  (define (get-name record) (cdar record))
  (define (get-address record) (cadr record))
  (define (get-salary record) (cddr record))
  
  ; the search in a list
  (define (get-record-by-id id file)
    (cond ((null? file) false)
          ((equal? id (get-id (car file)))
           (car file))
          (else (get-record-by-id id (cdr file)))))
  
  (define (get-record-by-name name file)
    (cond ((null? file) false)
          ((equal? name (get-name (car file)))
           (car file))
          (else (get-record-by-name name (cdr file)))))

  ;; interface to the rest of the system
  
  (define (tag x) (attach-tag 'div2 x))
  (put 'get-file 'div2 (tag (get-file)))
  (put 'get-id '(div2) get-id)
  (put 'get-name '(div2) get-name)
  (put 'get-address '(div2) get-address)
  (put 'get-salary '(div2) get-salary)
  (put 'get-record-by-id '(div2 div2) get-record-by-id)
  (put 'get-record-by-name '(div2 div2) get-record-by-name)
  )

(install-div1-package)
(install-div2-package)

(define (get-record-by-id id file)
  (let ((tag (lambda (x) (attach-tag (type-tag file) x))))
    (let ((record (apply-generic 'get-record-by-id (tag id) file)))
      (if record
          (tag record)
          false))))

(define (get-record-by-name name file)
  (let ((tag (lambda (x) (attach-tag (type-tag file) x))))
    (let ((record (apply-generic 'get-record-by-name (tag name) file)))
      (if record
          (tag record)
          false))))

(define (get-id record) (apply-generic 'get-id record))
(define (get-name record) (apply-generic 'get-name record))
(define (get-address record) (apply-generic 'get-address record))
(define (get-salary record) (apply-generic 'get-salary record))

(define (find-employee-record name filelist)
  (if (null? filelist)
      false
      (let ((record (get-record-by-name name (car filelist))))
        (if record
            record
            (find-employee-record name (cdr filelist))))))

;; tests

(define file1 (get 'get-file 'div1))
(define file2 (get 'get-file 'div2))

(define rec1 (get-record-by-id 2 file1))
rec1
(get-salary rec1)

(define rec2 (find-employee-record 'JimCarrey (list file1 file2)))
rec2
(get-salary rec2)

