#lang racket

(require math/base)

; recursive
(define (cont-frac D N count)
  (if (= count 0)
      0
      (/ (N count) (+ (D count) (cont-frac D N (- count 1))))))
  
; iterative
(define (iter-cont-frac D N count)
  (define (iter result count)
    (if (= count 0)
        result
        (iter (/ (N count) (+ (D count) result)) (- count 1))))
  (iter 0 count))

; tests

(cont-frac (lambda (i) 1.0) (lambda (i) 1.0) 11)
(iter-cont-frac (lambda (i) 1.0) (lambda (i) 1.0) 11)
(/ 1 phi.0) ; exact
