#lang racket

(define (for-each action items)
  (cond ((not (null? items))
         (action (car items)) ; action!
         (for-each action (cdr items)))))

; tests

(for-each
 (lambda (x) (display x) (newline))
 (list 57 321 88))
