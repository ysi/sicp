#lang racket

(require "common.scm")

(define (square-list items)
  (map square items))

(define (square-list-long items)
  (if (null? items)
      '()
      (cons
       (square (car items))
       (square-list-long (cdr items)))))

; tests

(square-list (list 1 2 3 4))
(square-list-long (list 1 2 3 4))