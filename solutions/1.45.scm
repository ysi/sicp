#lang racket

(require "common.scm")

(define tolerance 0.00001)
(define dx tolerance)

(define (fixed-point f first-guess)
  (define (close-enough? v1 v2)
    (< (abs (- v1 v2)) tolerance))
  (define (try guess)
    (let ((next (f guess)))
      (if (close-enough? guess next)
          next
          (try next))))
  (try first-guess))

(define (repeated f n)
  (if (= n 0)
      identity
      (compose f (repeated f (- n 1)))))

(define (testfun x n)
  (lambda (y) (/ x (expt y (- n 1)))))

(define (root n x)
  (fixed-point ((repeated average-damp (- n 1)) (testfun x n)) 1.0))

; tests

(expt (root 2 2) 2)
(expt (root 3 2) 3)
(expt (root 4 2) 4)
(expt (root 5 2) 5)
(expt (root 6 2) 6)
(expt (root 7 2) 7)
